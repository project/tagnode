by David Donohue (dado@drupal.org)

Tagnode Module
Note CVS (HEAD) version is for Drupal 4.7

SETUP
This module requires no changes to the database to function.  
Simply place a folder "tagnode" in your installation's modules folder, and place these files in it.
tagnode.module
tagnode.css

Go to administer -> modules and check on the tagnode module.

Additionally, since this module uses node titles and term names in order to tag nodes to other nodes, it seems 
prudent to add an index to the term_name column.  You can do so using the SQL in tagnode.mysql.  
Note the tagnode module functions without this index, it migh speed things up however.

OVERVIEW

This module permits node-to-node associations, using Drupal's taxonomy system. Administrators define 1 or more node types as being tagnode types. Tagnodes can be tagged to nodes of other types. When a taggee (i.e., a node that is tagged by a tagnode) is rendered, a link is also rendered to the tagnode(s) tagged to it.

Tagnodes facilitate 1-to-many or many-to-many relationships. For example, every event has 1 (or more) location; every music CD has 1 (or more) artist.

More example uses of this module include:

Taggee					Tagnode(s)
event					location, sponsor
music CD				artist, label
meeting				organizer, chapter
sport game			field

Tagnodes are paired with an identically named term from a particular vocabulary. That is, a tagnode = a node of a given type + an identically named term from a given vocabulary. The module uses the node title & term name as the means of linking the 2.

How to create a tagnode relationship:

   1. Create or identify the node types you need. You can use nodes of any type as the tagnode or as the taggee, but most commonly these nodes are new flexinode types. For example, create a flexinode type called 'event' as the taggee, and create a flexinode type called 'place' to be the tagnode.
   2. Create a new vocabulary for tagnode (go to administer -> categories -> add vocabulary). You can (but do not need to) give it the same name as the tagnode's node type. Check the box(es) of the node types to which the tagnode should be taggable. If you wish that every taggee MUST be tagged with the tagnode, then check the box 'Required'. If you wish that more than 1 tagnode of this type can be tagged to a given node (e.g. 2 or more artists can be credited to a music CD), then check the 'Multiple select' checkbox. In our example, create a new vocabulary named 'place', and check the box next to 'event' node type.
   3. Having laid the groundwork, tell the module to link the tagnode's node type and vocabulary. Go to administer -> settings -> content types, and click the node type you wish to convert into a tagnode (in our example, click 'place'.) Scroll to near the bottom of the form and find the 'Tagnode' configuration option. In the dropdown menu, select the vocabulary which defines the tagnode (in our example, select 'place'). Submit the form.

Once a tagnode is defined, users associate a taggee with a tagnode by selecting the tagnode's name in the dropdown select box in the taggee's form.

The module does the following actions:

    * When a new tagnode is created, it creates a term (in the tagnode-defining vocabulary) with the identical name.
    * When a tagnode's title is updated, the module updates the corresponding term.
    * When a tagnode is deleted, it deletes the term.
    * When a taggee is viewed, the tagnode(s) which have been tagged to it are also viewed. A link to the tagnode is displayed as well as the teaser or body of the tagnode.
    * When a taggee's form is rendered, a link is presented to create a new tagnode for each tagnode type it is categorized against.
    * When a tagnode is viewed as a teaser, a link is displayed to each of the node types to which it is taggable.
    * When a tagnode is viewed as a page, a list is shown for each node type, of the most recent n nodes to which this node is tagged.

At the admin -> settings -> tagnode maintenance page, you can find and fix tagnode-defining nodes and terms which are 'orphaned'. Orphaned nodes and terms can arise when nodes or terms are imported into the database, for example.  You can also use the tagnode maintentance area to convert already-existing nodes into tagnodes.

The module uses the hook_nodeapi() hook to perform its actions. Thus, it can be applied to nodes of any type. It uses no new tables. Since it uses the node title and the term name as a means to link tagnode-defining nodes to the terms, very little data is inserted into the existing tables. The names of tagnode-defining terms should not be changed in the administer -> categories section.

To remove this module, you can simply disable the module in administer -> modules, then delete any tagnode-defining vocabularies if you wish.  There is little residual footprint.

If you use the node_import module (or other method) to import tagnodes, or if you have already-existing nodes which you are converting into tagnodes, be sure to go to admin -> settings -> tagnode maintenance to add the corresponding terms for the tagnodes you have imported.

If you wish to have node types properly display as plural (as in 'see all stories' instead of 'see all story'), you might need to add your own implementation of the theme_type_name_display_tagnode() function, in your site's theme. The tagnode module contains a default implementation which properly pluraziles some node types.

Tagnode works well with freetagging.  I have found that Drupal 4.7's freetagging interface is well suited to entering tagnode information.  When the user enters a new taxonomy term (either through freetagging or through admin -> categories), a corresponding tagnode is created of the proper node type.  Naturally, this node has a title only (to match the newly-created term).

Tagnode has been updated to work with the Glossary module.  Glossary requires that a term's description field be populated, so when creating terms, Tagnode also populates the description field (with a text version of the node's teaser).  The consequence is that whenever tagnode titles appear in text, a link now appears, which can navigate the user to the tagnode.  This idea and documentation was contributed by barc@drupal.org.
1. Set up a new Flexinode type for Organisations. This contains contact
details and a summary of the work of the organisation.
2. Set up a vocabulary "Organisations". You can LEAVE EMPTY all the
checkboxes of the node types to which the tagnode should be taggable.
(You will not be manually associating tagees and tagnodes. Glossary
will do it automatically).
3. Link the Tagnode's node type and vocabulary as per standard Tagnode
instructions.
4. Set up the Glossary module as per standard instructions. Set the
vocabulary created above ("Oganisations") to be the one to use in Admin
-> Input Formats -> Configure -> Configure.
5. Glossary terms become links with mouseover description wherever they
are found within a node (of the permitted types) on the website. A
clickthrough will take you to the term description in the glossary
index. Also, if the term has any nodes associated with it, then those
nodes can be displayed also (or instead of the description). So, it
would be good if, when a Tagnode is created that a nodeterm record is
also created. For my situation it is very easy for me to do this
manually. 

CUSTOM THEMING

Please see documentation in the code for each of the functions beginning with "theme_".  
For use w/ PHPTemplate & others: I added the field
$node->tagnode_append
to contain all tagnode content
so I think in creating your PHPTemplate themes, you can reference a variable 
$tagnode_append

ROADMAP

At this writing, there are other modules in this space (such as Clipper module) and a project afoot to
create a Drupal core API for node-to-node relationships.  I hope to merge this module with those efforts.
I would expect the functional result would be more granular control of the node linking & labeling, so that
custom text could be displayed when linking from 1 node type to another.  Additionally there would 
probably be improved performance, and support for linking large sets of nodes.  

CHANGELOG
2005/11/3: made changes so that this module will work with PHP5
2005/11/9: fixed bug "SQL error when no vocabulary has been created" posted by aalex
2005/11/13: final? fixes for PHP5 & fixed bug prohibiting proper edit of nodes which are both tagnodes and taggees
2005/11/23: committing (stable?) 4.6 version to head (main) branch
2005/12/14: committing rough 4.7 version to head.  Head version of tagnode now is for Drupal 4.7
2006/1/8: fixed theoretical bug on loading taggee nodes, pointed out by hershmire here http://drupal.org/node/42988
2006/1/9: when creating or updating tagnodes, now populate term's description with the node's teaser (stripped of tags) or with node title if no teaser.  This permits some interoperability with the Glossary module.
2006/1/20: when user enters new taxonomy term, the corresponding tagnode is created (with only the title populated).  This supports freetagging, in which users can enter new terms.